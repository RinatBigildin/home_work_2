// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:home_work_2/extra_work/core/repository.dart';
import 'package:home_work_2/extra_work/model/weather_forecast_model.dart';
import 'package:home_work_2/extra_work/core/utils.dart';
import 'package:home_work_2/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    DateTime startDate = DateTime.utc(2022, 5, 11);
    DateTime endDate = DateTime.utc(2022, 12, 22);

    ServiceWeatherForecast.loadData(startDate, endDate);

    WeatherForecast weatherForecast = ServiceWeatherForecast.getData();

    var  currentDate = DateTime.now();

   var daysWeatherForecast = ServiceWeatherForecast.getWeatherForecastByTheDays();

    var currentWeatherForecast = ServiceWeatherForecast.getWeatherForecastByTheHourCurrentDay(currentDate);

   var  dayWeatherForecast = ServiceWeatherForecast.getWeatherForecastByTheClockCurrentDay(currentDate);

    var avg  =  CommonHelper.avgTemperatureForCurrentDay(weatherForecast.periodWeatherForecast, currentDate);

    // var weatherForecast_1  =
    // CommonHelper.currentTemperatureForCurrentDay(weatherForecast.periodWeatherForecast);
    // var weatherForecast_2  =
    // CommonHelper.avrgTemperatureForCurrentDay(weatherForecast.periodWeatherForecast);
    // var weatherForecast_3  =
    // CommonHelper.currentTemperatureForCurrentHour(weatherForecast.periodWeatherForecast);
    print(weatherForecast);
    // Build our app and trigger a frame.

    // await tester.pumpWidget(const MyApp());
    //
    // // Verify that our counter starts at 0.
    // expect(find.text('0'), findsOneWidget);
    // expect(find.text('1'), findsNothing);
    //
    // // Tap the '+' icon and trigger a frame.
    // await tester.tap(find.byIcon(Icons.add));
    // await tester.pump();
    //
    // // Verify that our counter has incremented.
    // expect(find.text('0'), findsNothing);
    // expect(find.text('1'), findsOneWidget);
  });
}
