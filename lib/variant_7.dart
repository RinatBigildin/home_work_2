import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Home Work 2 Variant 7",
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage()
    );
  }
}
class MyHomePage extends StatefulWidget {
  MyHomePage({key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MyHomePageState();
  }

}

class MyHomePageState extends State<MyHomePage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(
            "YouTube", style: TextStyle(fontSize: 22, color: Colors.white)),

        actions: [
          IconButton(
            icon: Icon(Icons.videocam_rounded, color: Colors.white,),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.search, color: Colors.white,),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.account_circle_rounded, color: Colors.white,),
            onPressed: () {},
          ),
        ],
        backgroundColor: Colors.red,
      ),
      body: ListView(
        children: listOfTitle,
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.red,
        currentIndex: this.selectedIndex,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Home"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.whatshot_rounded),
              label: "Trending"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.subscriptions_rounded),
              label: "Subscriptions"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.folder),
              label: "Library"
          ),
        ],
        onTap: (int index) {
          this.onTapHandler(index);
        },
      ),
    );
  }

  void onTapHandler(int index) {
    this.setState(() {
      this.selectedIndex = index;
    });
  }
}

final scaffoldKey = GlobalKey<ScaffoldState>();

TextStyle styleOfTextTile = TextStyle(fontSize: 14, color: Colors.black);
TextStyle styleOfTextSubTile = TextStyle(fontSize: 8, color: Colors.black);

List<ListTile> listOfTitle = [
  ListTile(
    title: Column(
                  children: [
                            Image.network("https://look.com.ua/pic/201808/800x600/look.com.ua-297769.jpg",
                            fit: BoxFit.fill,),
                            Align(alignment: Alignment.centerLeft, child: Text("Красивая природа", style: styleOfTextTile), )
                            ],
                  ),
    subtitle: Text("Красивые ролики онлайн", style: styleOfTextSubTile),
    onTap: () {},
  ),
  ListTile(
    title: Column(
      children: [
        Image.network("https://wallpapersmug.com/download/800x600/225378/retrowave-sunrise-reflection-clear-sky-4k.jpg",
          fit: BoxFit.fill,),
        Align(alignment: Alignment.centerLeft, child: Text("Рисуем пейзаж", style: styleOfTextTile), )
      ],
    ),
    subtitle: Text("Рисуем и монтируем вместе", style: styleOfTextSubTile),
    onTap: () {},
  ),
  ListTile(
    title: Column(
      children: [
        Image.network("https://images.wallpapersden.com/image/download/landscape-pixel-art_bGhnaGeUmZqaraWkpJRtZWWta2Vl.jpg",
          fit: BoxFit.fill,),
        Align(alignment: Alignment.centerLeft, child: Text("Слушаем новый альбом", style: styleOfTextTile), )
      ],
    ),
    subtitle: Text("Реторо музыка", style: styleOfTextSubTile),
    onTap: () {},
  ),
];

