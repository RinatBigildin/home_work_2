import 'dart:math';
import 'package:home_work_2/extra_work/core/utils.dart';
import 'package:home_work_2/extra_work/model/type_weather_enum.dart';
import 'package:home_work_2/extra_work/model/weather_forecast_model.dart';
import "package:collection/collection.dart";

class ServiceWeatherForecast {
  static late WeatherForecast _weatherForecastData;

  static late bool _loadData = false;

  static void loadData(DateTime startDate, DateTime endDate) {
    _weatherForecastData = _getWeatherForecast(startDate, endDate);
    _loadData = true;
  }

  static bool get IsLoadData => _loadData;

  static WeatherForecast getData() {
    return _weatherForecastData;
  }

  static DayWeatherForecast? getWeatherForecastByTheClockCurrentDay(DateTime currentDate) {
    if (IsLoadData) {
      DayWeatherForecast? currentWeatherForDay = getData().periodWeatherForecast
          .firstWhereOrNull((element) =>
      element.dateDay.year == currentDate.year
          &&
          element.dateDay.month == currentDate.month
          &&
          element.dateDay.day == currentDate.day
      );
      return currentWeatherForDay;
    }
    else {
      return null;
    }
  }

  static HourWeatherForecast? getWeatherForecastByTheHourCurrentDay(DateTime currentDate) {
    if (IsLoadData) {
      var currentWeatherForDay = getData().periodWeatherForecast
          .firstWhereOrNull((element) =>
      element.dateDay.year == currentDate.year
          &&
          element.dateDay.month == currentDate.month
          &&
          element.dateDay.day == currentDate.day
      );
      if (currentWeatherForDay != null) {
        var hourWeatherForecast = currentWeatherForDay
            .hourWeatherForecast
            .firstWhereOrNull((element) => element.hour == currentDate.hour);

        if (hourWeatherForecast != null) {
          return hourWeatherForecast;
        }
      }
    }
    return null;
  }

  static List<DayWeatherForecast> getWeatherForecastByTheDays() {
    if (IsLoadData) {
      return getData().periodWeatherForecast;
    }
    else {
      return List<DayWeatherForecast>.empty();
    }
  }


  static WeatherForecast _getWeatherForecast(DateTime startDate, DateTime endDate){
    int countDay = CommonHelper.daysBetween(startDate, endDate);
    int countMonth = CommonHelper.monthBetween(startDate, endDate);
    List<DateTime> days = CommonHelper.getDayForRangeDate(startDate, endDate);
    List<DayWeatherForecast> periodWeatherForecast = [];
    days.forEach((element) {
      List<HourWeatherForecast> hourWeatherForecast = [];
      for(int i=0; i<24; i++) {
        hourWeatherForecast.add(new HourWeatherForecast(i, _generateRandom(element.month)));
      }
      periodWeatherForecast.add(DayWeatherForecast(element,hourWeatherForecast));
    });

    return WeatherForecast("Самара", "ru"
        , startDate
        , endDate
        , countDay
        , countMonth
        , periodWeatherForecast
    );
  }
  static Map<int,Map<String,int>> _data = Map();

  static Map<int,Map<String,int>> _getData(){
    if (_data.isEmpty)
      {
        _data[1] = new Map();
        _data[1]!["temperatura"] =  -30;
        _data[1]!["experiences"] = -30;
        _data[1]!["wind"] =          20;
        _data[1]!["pressure"] =     90;
        _data[1]!["humidity"] =     100;

        _data[2] = new Map();
        _data[2]!["temperatura"] =  -20;
        _data[2]!["experiences"] =  -20;
        _data[2]!["wind"] =         20;
        _data[2]!["pressure"] =     90;
        _data[2]!["humidity"] =     100;

        _data[3] = new Map();
        _data[3]!["temperatura"] =  -15;
        _data[3]!["experiences"] =  -15;
        _data[3]!["wind"] =         20;
        _data[3]!["pressure"] =     90;
        _data[3]!["humidity"] =     100;

        _data[4] = new Map();
        _data[4]!["temperatura"] =  -10;
        _data[4]!["experiences"] =  -10;
        _data[4]!["wind"] =         20;
        _data[4]!["pressure"] =     90;
        _data[4]!["humidity"] =     100;

        _data[5] = new Map();
        _data[5]!["temperatura"] =  15;
        _data[5]!["experiences"] =  15;
        _data[5]!["wind"] =         20;
        _data[5]!["pressure"] =     90;
        _data[5]!["humidity"] =     100;

        _data[6] = new Map();
        _data[6]!["temperatura"] =  26;
        _data[6]!["experiences"] =  26;
        _data[6]!["wind"] =         20;
        _data[6]!["pressure"] =     90;
        _data[6]!["humidity"] =     100;

        _data[7] = new Map();
        _data[7]!["temperatura"] =  32;
        _data[7]!["experiences"] =  32;
        _data[7]!["wind"] =         20;
        _data[7]!["pressure"] =     90;
        _data[7]!["humidity"] =     100;

        _data[8] = new Map();
        _data[8]!["temperatura"] =  35;
        _data[8]!["experiences"] =  35;
        _data[8]!["wind"] =         20;
        _data[8]!["pressure"] =     90;
        _data[8]!["humidity"] =     100;

        _data[9] = new Map();
        _data[9]!["temperatura"] =  6;
        _data[9]!["experiences"] =  6;
        _data[9]!["wind"] =         20;
        _data[9]!["pressure"] =     90;
        _data[9]!["humidity"] =     100;

        _data[10] = new Map();
        _data[10]!["temperatura"] =  -5;
        _data[10]!["experiences"] =  -5;
        _data[10]!["wind"] =         20;
        _data[10]!["pressure"] =     90;
        _data[10]!["humidity"] =     100;

        _data[11] = new Map();
        _data[11]!["temperatura"] =  -10;
        _data[11]!["experiences"] =  -10;
        _data[11]!["wind"] =         20;
        _data[11]!["pressure"] =     90;
        _data[11]!["humidity"] =     30;

        _data[12] = new Map();
        _data[12]!["temperatura"] =  -20;
        _data[12]!["experiences"] =  -20;
        _data[12]!["wind"] =         20;
        _data[12]!["pressure"] =     90;
        _data[12]!["humidity"] =     30;
      }
    return _data;
  }

  static Weather _generateRandom(int month){
    Random random = Random();
    Map<int,Map<String,int>> data = _getData();
    double temperatura =  random.nextDouble()* data[month]!["temperatura"]!;
    double experiences = random.nextDouble()* data[month]!["experiences"]!;
    double wind = random.nextDouble()* data[month]!["wind"]!;
    double pressure = random.nextDouble()* data[month]!["pressure"]! + 700;
    double humidity = random.nextDouble()* data[month]!["humidity"]!;
     int rndTypeWeather = random.nextInt(5)+1;
     TypeWeather typeWeather = TypeWeather.values[rndTypeWeather];
    return Weather(temperatura, experiences, wind, pressure, humidity, typeWeather);
  }

}