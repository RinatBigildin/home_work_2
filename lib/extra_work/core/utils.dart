import 'package:home_work_2/extra_work/model/type_weather_enum.dart';
import 'package:home_work_2/extra_work/model/weather_forecast_model.dart';
import "package:collection/collection.dart";

class CommonHelper {
  static String getLocalizeWeekday(int weekday)
  {
     String result = "";
     switch(weekday){
       case(1):
         result = "Пн";
         break;
       case(2):
         result = "Вт";
         break;
       case(3):
         result = "Ср";
         break;
       case(4):
         result = "Чт";
         break;
       case(5):
         result = "Пт";
         break;
       case(6):
         result = "Сб";
         break;
       case(7):
         result = "Вс";
         break;
     }
     return result;
  }
  static String getLocalizeMonth(int month)
  {
    String result = "";
    switch(month){
      case(1):
        result = "янв";
        break;
      case(2):
        result = "фев";
        break;
      case(3):
        result = "мар";
        break;
      case(4):
        result = "апр";
        break;
      case(5):
        result = "май";
        break;
      case(6):
        result = "июн";
        break;
      case(7):
        result = "июл";
        break;
      case(8):
        result = "авг";
        break;
      case(9):
        result = "сен";
        break;
      case(10):
        result = "окт";
        break;
      case(11):
        result = "ноя";
        break;
      case(12):
        result = "дек";
        break;
    }
    return result;
  }

  static String getStrDate(DateTime dateDay)
  {
    var weekday = CommonHelper.getLocalizeWeekday(dateDay.weekday);
    var month = CommonHelper.getLocalizeMonth(dateDay.month);
    var day = dateDay.day.toString();
    var year = dateDay.year.toString();
    return "$weekday, $day $month $year год";
  }
  static String getStrHour(int hour) => "$hour:00" ;

  static String getStrTime(int hour, int minute) => "$hour:$minute" ;

  static int daysBetween(DateTime from, DateTime to) {
    from = DateTime.utc(from.year, from.month, from.day);
    to = DateTime.utc(to.year, to.month, to.day);
    return to.difference(from).inDays;
  }
  static int monthBetween(DateTime from, DateTime to) {
    final firstDate = to;
    final secondDate = from;
    final monthsDifference = (firstDate.year - secondDate.year) * 12 +
        firstDate.month - secondDate.month;
    return monthsDifference;
  }

  static List<DateTime> getDayForRangeDate(DateTime startDate, DateTime endDate) {
    List<DateTime> days = [];
    // DateTime tmp = DateTime(startDate.year, startDate.month, startDate.day, 12);
    for (int i = 0; i <= endDate.difference(startDate).inDays; i++) {
      days.add(startDate.add(Duration(days: i)));
    }
    return days;
  }

  static Weather? avgTemperatureForCurrentDay (List<DayWeatherForecast> weatherPeriod, DateTime currentDate) {
    DayWeatherForecast? currentWeatherForDay = weatherPeriod.firstWhereOrNull((
        element) =>
    element.dateDay.year == currentDate.year
        &&
        element.dateDay.month == currentDate.month
        &&
        element.dateDay.day == currentDate.day
    );
    if (currentWeatherForDay != null) {
      List<HourWeatherForecast> weatherForDay = currentWeatherForDay.hourWeatherForecast;

      double allTemperature = 0;
      double allExperiences = 0;
      double allWind = 0;
      double allPressure = 0;
      double allHumidity = 0;
      int  allTypeWeather = 0;
      int count = weatherForDay.length;
      TypeWeather typeWeather = TypeWeather.empty;
      weatherForDay.forEach((element) {
        allTemperature += element.temperature;
        allExperiences += element.experiences;
        allWind += element.wind;
        allPressure += element.pressure;
        allHumidity += element.humidity;
         if (element.hour == 12) typeWeather = element.typeWeather;
      });
      return Weather(allTemperature / count,
          allExperiences / count,
          allWind / count,
          allPressure / count,
          allHumidity / count,
          typeWeather
      );
    }
    else {
      return null;
    }
  }
}
