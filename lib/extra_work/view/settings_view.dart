import 'package:flutter/material.dart';

class SettingsView {
  SettingsView._();

  // static const Color COLOR_BACKGROUND_HOMEPAGE = Colors.white;

  /// Заголовок
  static late Widget TITLE =
  Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: const [
        Text("Погода",
            style: TextStyle(fontSize: 24, color: Colors.white),
            textAlign: TextAlign.center),
      ]
  );

  ///AppBar
  static const Color COLOR_ICON_APPBAR = Colors.white;
  static const Color COLOR_BACKGROUND_APPBAR = Colors.blueGrey;

  /// TabAppBar
  static const StatelessWidget ICON_TABBAR_1 = Icon(
      Icons.schedule, color: COLOR_ICON_APPBAR);
  static const StatelessWidget ICON_TABBAR_2 = Icon(
      Icons.cloud, color: COLOR_ICON_APPBAR);
  static const StatelessWidget ICON_TABBAR_3 = Icon(
      Icons.list, color: COLOR_ICON_APPBAR);

  static const Tab TAB_1 = Tab(icon: ICON_TABBAR_1,);
  static const Tab TAB_2 = Tab(icon: ICON_TABBAR_2,);
  static const Tab TAB_3 = Tab(icon: ICON_TABBAR_3,);

  /// Leading
  static const Widget ICON_LEADING = Icon(Icons.refresh, color: COLOR_ICON_APPBAR);
  /// Actions
  static const Widget ICON_ACTIONS = Icon(Icons.location_on, color: COLOR_ICON_APPBAR);

  /// Body
  static const Color COLOR_BACKGROUND_BODY = Colors.black12;

}


