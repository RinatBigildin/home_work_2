import 'package:home_work_2/extra_work/model/type_weather_enum.dart';
import 'package:flutter/material.dart';

class CommonHelperView {

  static Image getImageCaseTypeWeather(TypeWeather typeWeather, double width, double height)
  {
    switch(typeWeather) {
      case(TypeWeather.clear_in_the_afternoon): return Image.network("https://cdn-icons-png.flaticon.com/128/4735/4735121.png", width: width, height: height);
      case(TypeWeather.clear_at_night): return Image.network("https://cdn-icons-png.flaticon.com/128/4735/4735065.png", width: width, height: height);
      case(TypeWeather.cloudy): return Image.network("https://cdn-icons-png.flaticon.com/128/4735/4735030.png", width: width, height: height);
      case(TypeWeather.cloudy_with_clearing): return Image.network("https://cdn-icons-png.flaticon.com/128/4735/4735030.png", width: width, height: height);
      case(TypeWeather.rain): return Image.network("https://cdn-icons-png.flaticon.com/128/4735/4735146.png", width: width, height: height);
      case(TypeWeather.rain_with_thunderstorm): return Image.network("https://cdn-icons-png.flaticon.com/128/4735/4735113.png", width: width, height: height);
      case(TypeWeather.dull): return Image.network("https://cdn-icons-png.flaticon.com/128/899/899681.png", width: width, height: height);
      case(TypeWeather.snow): return Image.network("https://cdn-icons-png.flaticon.com/128/899/899681.png", width: width, height: height);
      case(TypeWeather.empty): return Image.network("https://cdn.icon-icons.com/icons2/47/PNG/128/unknown_empty_9802.png", width: width, height: height);
      default: return Image.network("https://cdn.icon-icons.com/icons2/47/PNG/128/unknown_empty_9802.png", width: width, height: height);
    }
  }
}