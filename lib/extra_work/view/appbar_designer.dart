import 'package:flutter/material.dart';
import 'package:home_work_2/extra_work/view/settings_view.dart';

class AppBarDesignWeather extends StatelessWidget with PreferredSizeWidget{
  @override
  Widget build(BuildContext context) {
    return AppBar(
        leading: IconButton(
            icon: SettingsView.ICON_LEADING,
            onPressed: () {}
        ),
        actions: [
          IconButton(
            icon: SettingsView.ICON_ACTIONS,
            onPressed: () {},
          ),
        ],
        title: SettingsView.TITLE,
        centerTitle: true,
        backgroundColor: SettingsView.COLOR_BACKGROUND_APPBAR,
        bottom:TabBar(tabs: [ SettingsView.TAB_1, SettingsView.TAB_2, SettingsView.TAB_3],
            indicatorColor: Colors.white
        )
    );
  }
  @override
  Size get preferredSize => Size.fromHeight(100);
}
