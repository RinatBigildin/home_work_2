import 'package:flutter/material.dart';
import 'package:home_work_2/extra_work/core/utils.dart';
import 'package:home_work_2/extra_work/view/settings_view.dart';
import 'package:home_work_2/extra_work/core/repository.dart';
import 'package:home_work_2/extra_work/model/type_weather_enum.dart';
import 'package:home_work_2/extra_work/view/utils_view.dart';

class CurrentWeather extends StatelessWidget {
  CurrentWeather();

  var currentDate = DateTime.now();
  var weatherForecast= ServiceWeatherForecast.getData();
  var currentWeather = ServiceWeatherForecast.getWeatherForecastByTheHourCurrentDay(DateTime.now());

  @override
  Widget build(BuildContext context) {

    String day = CommonHelper.getStrDate(currentDate);
    String time = CommonHelper.getStrTime(currentDate.hour, currentDate.minute);
    String title = weatherForecast.strCity;
    String temperatura = currentWeather?.strTemperatura ?? "";
    String experiences = currentWeather?.strExperiences ?? "";
    String wind = currentWeather?.strWind ?? "";
    String pressure = currentWeather?.strPressure ?? "";
    String humidity = currentWeather?.strHumidity ?? "";
    TypeWeather typeWeather = currentWeather?.typeWeather ?? TypeWeather.empty;

    return Container(
       padding:  EdgeInsets.fromLTRB(0, 80, 0, 0),
       color: SettingsView.COLOR_BACKGROUND_BODY,
      alignment: Alignment.center,
      child: Column(

        children: [
          Text(title, style: TextStyle(fontSize: 32, color: Colors.black, fontWeight: FontWeight.bold ),),
          SizedBox(height: 20),
          Text("Прогноз обновлен:", style: TextStyle(fontSize: 16, color: Colors.black)),
          Text(day, style: TextStyle(fontSize: 24, color: Colors.black ),),
          Text(time, textAlign: TextAlign.center, style: TextStyle(fontSize: 24)),
          SizedBox(height: 10),
          CommonHelperView.getImageCaseTypeWeather(typeWeather, 200, 200),
          SizedBox(height: 10),
          Text(temperatura, style: TextStyle(fontSize: 42, color: Colors.black ),),
          SizedBox(height: 30),
          Container(height: 10, color: Colors.black12 ),
          SizedBox(height: 20),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              RowTypeWeather("Ветер", wind),
              RowTypeWeather("Влажность", humidity),
            ]
          ),
          SizedBox(height: 20),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              RowTypeWeather("Давление", pressure),
              RowTypeWeather("Ощущение", experiences),
            ]
          ),
        ],
      ),
    );
  }
}

class RowTypeWeather extends StatelessWidget {
  RowTypeWeather(String this.label, String this.data, {super.key});
  final String label;
  final String data;
  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            width: 110,
            child: Text(this.label,
                style: TextStyle(fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic)),
          ),
          Container(
            width: 80,
            child: Text(this.data, style: TextStyle(fontSize: 18)),
          ),
        ]);
  }
}