import 'package:flutter/material.dart';
import 'package:home_work_2/extra_work/view/application.dart';

void main() {
  DateTime startDate = DateTime.utc(2022, 05, 01);
  DateTime endDate = DateTime.utc(2022, 12, 31);

  runApp(MainApp(startDate, endDate));
}


