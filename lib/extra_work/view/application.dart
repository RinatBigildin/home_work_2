import 'package:flutter/material.dart';
import 'package:home_work_2/extra_work/view/homepage_designer.dart';
import 'package:home_work_2/extra_work/core/repository.dart';

class MainApp extends StatelessWidget {
  MainApp(DateTime this.startDate, DateTime this.endDate);

  final DateTime startDate;
  final DateTime endDate;

  @override
  Widget build(BuildContext context) {

    ServiceWeatherForecast.loadData(this.startDate, this.endDate);

    return MaterialApp(
        title: "Home Work 2 Variant 7",
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: HomePageWeather()
    );
  }

}