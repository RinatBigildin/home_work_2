import 'package:flutter/material.dart';
import 'package:home_work_2/extra_work/view/tab_clock_weather.dart';
import 'package:home_work_2/extra_work/view/tab_currentday_weather.dart';
import 'package:home_work_2/extra_work/view/tab_days_weather.dart';

class BodyDesignWeather extends StatelessWidget{
  BodyDesignWeather();

  @override
  Widget build(BuildContext context) {
    return TabBarView(
            children: [
              Center(child: ClockWeather()),
              Center(child: CurrentWeather()),
              Center(child: DayWeather())
            ]);
  }
}