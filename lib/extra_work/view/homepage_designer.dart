import 'package:flutter/material.dart';
import 'package:home_work_2/extra_work/view/settings_view.dart';
import 'package:home_work_2/extra_work/view/appbar_designer.dart';
import 'package:home_work_2/extra_work/view/body_designer.dart';
import 'package:home_work_2/extra_work/model/weather_forecast_model.dart';


class HomePageWeather extends StatefulWidget {
  HomePageWeather({key}) : super(key: key);


  @override
  State<StatefulWidget> createState() {
    return HomePageWeatherState();
  }
}
class HomePageWeatherState extends State<HomePageWeather> {
  HomePageWeatherState({key});

  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBarDesignWeather(),
          body: BodyDesignWeather()
      ),
    );

  }
}
