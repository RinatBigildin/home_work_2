import 'package:flutter/material.dart';
import 'package:home_work_2/extra_work/core/repository.dart';
import 'package:home_work_2/extra_work/model/type_weather_enum.dart';
import 'package:home_work_2/extra_work/view/settings_view.dart';
import 'package:home_work_2/extra_work/model/weather_forecast_model.dart';
import 'package:home_work_2/extra_work/view/utils_view.dart';

class ClockWeather extends StatelessWidget {
  ClockWeather();

  late DayWeatherForecast? dayWeatherForecast = ServiceWeatherForecast.getWeatherForecastByTheClockCurrentDay(currentDate);
  late DateTime  currentDate = DateTime.now();


  @override
  void initState() {

  }

  @override
  Widget build(BuildContext context) {

    if (dayWeatherForecast == null) {
      return Container(
        alignment: Alignment.center,
        color: SettingsView.COLOR_BACKGROUND_BODY,
        child: Text("Осутствуют данные за текущий день",
          style: TextStyle(fontSize: 24, color: Colors.black),),
      );
    }
    else {
      return Container(
          padding: EdgeInsets.fromLTRB(50, 30, 50, 30),
          color: SettingsView.COLOR_BACKGROUND_BODY,
          alignment: Alignment.center,
          child: this.dayWeatherForecast?.hourWeatherForecast.length == 0 ?
              const Center(
                child: Text("Осутствуют данные за текущий день",
                  style: TextStyle(fontSize: 24, color: Colors.black)),
              )
              : ListView.builder(
              itemCount: this.dayWeatherForecast?.hourWeatherForecast.length,
              itemBuilder: (_, int index) {
                String day = this.dayWeatherForecast?.strDateDay?? "";
                String hour = this.dayWeatherForecast?.hourWeatherForecast.elementAt(index).strHour?? "";
                String temperatura = this.dayWeatherForecast?.hourWeatherForecast.elementAt(index).strTemperatura?? "";
                String experiences = this.dayWeatherForecast?.hourWeatherForecast.elementAt(index).strExperiences?? "";
                String wind = this.dayWeatherForecast?.hourWeatherForecast.elementAt(index).strWind?? "";
                String pressure = this.dayWeatherForecast?.hourWeatherForecast.elementAt(index).strPressure?? "";
                String humidity = this.dayWeatherForecast?.hourWeatherForecast.elementAt(index).strHumidity?? "";
                TypeWeather typeWeather = this.dayWeatherForecast?.hourWeatherForecast.elementAt(index).typeWeather?? TypeWeather.empty;
                return
                  Column(
                    children: [
                      RowDayWeather(day, typeWeather),
                      RowHourWeather(hour),
                      SizedBox(height: 10),
                      RowTypeWeather("Температура", temperatura),
                      SizedBox(height: 5),
                      RowTypeWeather("Ощущение", experiences),
                      SizedBox(height: 5),
                      RowTypeWeather("Ветер", wind),
                      SizedBox(height: 5),
                      RowTypeWeather("Давление", pressure),
                      SizedBox(height: 5),
                      RowTypeWeather("Влажность", humidity),
                      SizedBox(height: 30),
                    ],
                  );
              }
          )
      );
    }
  }
}
class RowDayWeather extends StatelessWidget {
  RowDayWeather(String this.day, TypeWeather this.typeWeather);
  final String day;
  final TypeWeather typeWeather;

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size; //.width * 0.7;
    return  Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(day, style: TextStyle(fontSize: 24, color: Colors.black)),
        CommonHelperView.getImageCaseTypeWeather(typeWeather, 50, 50)
      ],
    );
  }
}
class RowHourWeather extends StatelessWidget {
  RowHourWeather(String this.hour);
  final String hour;
  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [Text(hour, textAlign: TextAlign.center, style: TextStyle(fontSize: 24)) ]
    );
  }
}

class RowTypeWeather extends StatelessWidget {
  RowTypeWeather(String this.label, String this.data, {super.key});
  final String label;
  final String data;
  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            width: 200,
            child: Text(this.label,
                style: TextStyle(fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic)),
          ),
          Container(
            width: 80,
            child: Text(this.data, style: TextStyle(fontSize: 18)),
          ),
        ]);
  }
}