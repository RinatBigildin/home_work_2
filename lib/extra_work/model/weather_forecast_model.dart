import 'package:home_work_2/extra_work/core/utils.dart';
import 'package:home_work_2/extra_work/model/type_weather_enum.dart';

class Weather {
  Weather(
      this.temperatura,
      this.experiences,
      this.wind,
      this.pressure,
      this.humidity,
      this.typeWeather,
      );

  ///темпереатура
  final double temperatura; // темпереатура
  /// ощущение
  final double experiences; // ощущение
  /// ветер
  final double wind;        // ветер
  /// давление
  final double pressure;    // давление
  /// влажность
  final double humidity;    // влажность

  final TypeWeather typeWeather; // тип погода


  String get strTemperatura {
    return "${temperatura.toStringAsFixed(2)} С°";
  }
  String get strExperiences {
    return "${experiences.toStringAsFixed(2)} С°";
  }
  String get strWind{
    return "${wind.toStringAsFixed(2)} м/с";
  }
  String get strPressure{
    return "${pressure.round()} мм";
  }
  String get strHumidity{
    return "${humidity.toStringAsFixed(2)} %";
  }
}

class HourWeatherForecast {
  HourWeatherForecast(
      this.hour,
      this.weather
      );
  final int hour;
  final Weather weather;
  double get temperature => weather.temperatura;
  double get experiences => weather.experiences;
  double get wind => weather.wind;
  double get pressure => weather.pressure;
  double get humidity => weather.humidity;

  String get strTemperatura => weather.strTemperatura;
  String get strExperiences => weather.strExperiences;
  String get strWind => weather.strWind;
  String get strPressure => weather.strPressure;
  String get strHumidity => weather.strHumidity;
  //TypeWeather get typeWeather => weather.typeWeather;
  TypeWeather get typeWeather {
    int result = weather.typeWeather.index;

    if (result == 1 || result == 2) {
      if ((hour >= 22 && hour <= 23) ||  (hour >= 0 && hour <= 5)) {
        result = 2;
      }
      else {
        result = 1;
      }
    }

    return (TypeWeather.values[result]);
  }

  String get strHour => CommonHelper.getStrHour(hour);

}

class DayWeatherForecast {
  DayWeatherForecast(
      this.dateDay,
      this.hourWeatherForecast
      );

  final DateTime dateDay;
  final List<HourWeatherForecast> hourWeatherForecast;

  String get strDateDay => CommonHelper.getStrDate(dateDay);
}

class WeatherForecast {
  WeatherForecast(
      this.city,
      this.locale,
      this.startDate,
      this.endDate,
      this.countDay,
      this.countMonth,
      this.periodWeatherForecast
      );

  final DateTime startDate;
  final DateTime endDate;
  final int countMonth;
  final int countDay;
  final String city;
  final String locale;
  final List<DayWeatherForecast> periodWeatherForecast;


  String get strCity {
    return "$city, $locale";
  }

}