enum TypeWeather {
  /// пусто
  empty,

  /// ясно днем
  clear_in_the_afternoon,

  /// ясно ночью
  clear_at_night,

  /// облачно с прояснением
  cloudy_with_clearing,

  ///облачно
  cloudy,

  /// дождь
  rain,

  /// дождь с грозой
  rain_with_thunderstorm,

  /// пасмурно
  dull,

  /// снег
  snow,
}
// ясно днем https://cdn-icons-png.flaticon.com/128/4735/4735121.png
// ясно ночью https://www.flaticon.com/free-icon/moon_4735065
// облачно с прояснением https://cdn-icons-png.flaticon.com/128/4735/4735030.png
// облачно https://cdn-icons-png.flaticon.com/128/4735/4735025.png
// дождь https://cdn-icons-png.flaticon.com/128/4735/4735146.png
// дождь с грозой https://cdn-icons-png.flaticon.com/128/4735/4735113.png
// пасмурно  https://cdn-icons-png.flaticon.com/128/899/899681.png
// снег https://www.flaticon.com/free-icon/snowflake_4735107
//

