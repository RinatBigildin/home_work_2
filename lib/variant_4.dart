import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Home Work 2 Variant 4",
        theme: ThemeData(
          primarySwatch: Colors.grey,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage()
    );
  }
}
class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        leading: IconButton(
          icon:Icon(Icons.list, color: Colors.white,),
          onPressed: () {
            if(scaffoldKey.currentState!.isDrawerOpen){
              scaffoldKey.currentState!.closeDrawer();
              //close drawer, if drawer is open
            }else{
              scaffoldKey.currentState!.openDrawer();
              //open drawer, if drawer is closed
            }
          },
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.add, color: Colors.white,),
            onPressed: (){},
          ),
        ],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Плавающие кнопки",
                style: TextStyle(fontSize: 24, color: Colors.white),
                textAlign: TextAlign.center),
            Padding(padding:  const EdgeInsets.only(left: 5.0),
              child: Icon(Icons.computer_outlined, color: Colors.white) ,
            )
          ],
        ),
        centerTitle: true,
        backgroundColor: colorButton,
      ),
      drawer: Drawer(
        child: ListView(
          children: getlistOfDraw(),
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        verticalDirection: VerticalDirection.down,
        children: [
          Padding(padding:  const EdgeInsets.only(bottom: 10.0),
            child: FloatingActionButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.zero)),
                  onPressed: () {},
                  child: Icon(Icons.stop, color: Colors.white, size: 40,),
                  backgroundColor: Colors.blueGrey,
              )
          ),
          FloatingActionButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50))),
            onPressed: () {},
            child: Icon(Icons.fiber_manual_record, color: Colors.white, size: 40,),
            backgroundColor: Colors.blueGrey,
          ),
        ],
      ),
      body: Align(alignment: Alignment.center,
        child: Icon(Icons.settings, color: colorButton, size: 30),
      )
    );
  }
}
final scaffoldKey = GlobalKey<ScaffoldState>();

TextStyle styleOfTextTile = TextStyle(fontSize: 12);
Color colorButton = Colors.blueGrey;

List<Widget> getlistOfDraw() {
  List<Widget> list = listOfTitle;
  return list;
}

List<ListTile> listOfTitle = [
  ListTile(
    title: Text("Домой", style: styleOfTextTile),
    onTap: () {},
  ),
  ListTile(title: Text("Настройки", style: styleOfTextTile),
    onTap: () { },
  ),
];

