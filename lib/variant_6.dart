import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Home Work 2 Variant 6",
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage()
    );
  }
}
class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
        appBar: AppBar(
            leading: IconButton(
            icon: Icon(Icons.delete, color: Colors.white,),
            onPressed: () {}
            ),
            actions: [
            IconButton(
            icon: Icon(Icons.add, color: Colors.white,),
            onPressed: () {},
            ),
            ],
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("Вкладки",
                style: TextStyle(fontSize: 24, color: Colors.white),
                textAlign: TextAlign.center),
                  Padding(padding: const EdgeInsets.only(left: 5.0),
              child: Icon(Icons.list_alt, color: Colors.white),
            )
          ],
        ),
        centerTitle: true,
        backgroundColor: colorButton,
          bottom:TabBar(tabs: [
                          Tab(icon: Icon(Icons.home)),
                          Tab(icon: Icon(Icons.work)),
                          Tab(icon: Icon(Icons.notifications)),
                        ],
                        indicatorColor: Colors.white
                        )
      ),

      body: TabBarView(
        children: [
              Center(child: Icon(Icons.home)),
              Center(child: Icon(Icons.work)),
              Center(child: Icon(Icons.notifications))
        ],
      )
    ),
    );
  }
}
TextStyle styleOfTextTile = TextStyle(fontSize: 12);
Color colorButton = Colors.blueGrey;


