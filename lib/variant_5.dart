import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Home Work 2 Variant 5",
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage()
    );
  }
}
class MyHomePage extends StatelessWidget {
  FocusNode fio = FocusNode();
  FocusNode university = FocusNode();
  FocusNode logon =FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.library_add, color: Colors.white,),
            onPressed: () {}
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.add, color: Colors.white,),
            onPressed: () {},
          ),
        ],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Поля логина",
                style: TextStyle(fontSize: 24, color: Colors.white),
                textAlign: TextAlign.center),
            Padding(padding: const EdgeInsets.only(left: 5.0),
              child: Icon(Icons.exit_to_app, color: Colors.white),
            )
          ],
        ),
        centerTitle: true,
        backgroundColor: colorButton,
      ),
      body:
      Align(alignment: Alignment.center,
        child:
    Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextInput( labelText: "Введите фамилию и имя",
                hintText: "Введите фамилию и имя",
                helperText: "Внимание! Заполните без ошибок!",
                icon: Icons.mood,
                fontSize: 12,
                currentFocusNode: fio,
                nextFocusNode: university,
              ),
              SizedBox(height: 20,),
              TextInput( labelText: "Введите университет",
                hintText: "Введите университет",
                helperText: "Внимание! Заполните без ошибок!",
                icon: Icons.school,
                fontSize: 12,
                currentFocusNode: university,
                nextFocusNode: logon,
              ),
              SizedBox(height: 20,),
              TextButton(
              focusNode: logon,
              child: Text("Войти"),
              onPressed: (){},
              ),
            ]
        ),
       ),
      ),
    );
  }
}
class TextInput extends StatefulWidget {
  TextInput({key, this.labelText,
  this.hintText,
  this.helperText,
  this.icon,
  this.maxLength,
  this.fontSize,
  this.stateColor,
    this.currentFocusNode,
    this.nextFocusNode,
  }): super(key: key);

  final String? labelText;
  final String? hintText;
  final String? helperText;
  final IconData? icon;
  final int? maxLength;
  final double? fontSize;
  final Color? stateColor;
  final FocusNode? currentFocusNode;
  final FocusNode? nextFocusNode;


  @override
  TextInputState createState() => TextInputState(
                                                  labelText: this.labelText,
                                                  hintText: this.hintText,
                                                  helperText: this.helperText,
                                                  icon: this.icon,
                                                  maxLength: this.maxLength,
                                                  fontSize: this.fontSize,
                                                  stateColor: this.stateColor,
                                                  currentFocusNode: this.currentFocusNode,
                                                  nextFocusNode: this.nextFocusNode
                                                );
}

class TextInputState extends State<TextInput> {

  TextInputState(
      {
        this.labelText,
        this.hintText,
        this.helperText,
        this.icon,
        this.maxLength,
        this.fontSize,
        this.stateColor,
        this.currentFocusNode,
        this.nextFocusNode,
  });
  final String? labelText;
  final String? hintText;
  final String? helperText;
  final IconData? icon;
  final int? maxLength;
  final double? fontSize;
  final Color? stateColor;
  final FocusNode? nextFocusNode;
  final FocusNode? currentFocusNode;


  @override
  Widget build(BuildContext context) {
    return TextFormField( style: TextStyle(fontSize: this.fontSize==null? 14: this.fontSize, color: Colors.black),

        enabled: true,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        maxLength: this.maxLength==null? 20: this.maxLength,
        focusNode: this.currentFocusNode,
        decoration: InputDecoration(
            border: OutlineInputBorder(borderSide: BorderSide(color: Colors.teal)),
            labelText: this.labelText,
            hintText: this.hintText,
            helperText: this.helperText,
            icon: this.icon == null? null: Icon(this.icon),
            contentPadding: const EdgeInsets.all(10.0) ),
            onFieldSubmitted: (term) {
                  if (this.nextFocusNode != null)
                  {
                    this.currentFocusNode?.unfocus();
                    FocusScope.of(context).requestFocus(this.nextFocusNode);
                  }
            },
    );
  }
}
TextStyle styleOfTextTile = TextStyle(fontSize: 12);
Color colorButton = Colors.blueGrey;


