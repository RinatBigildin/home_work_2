import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Home Work 2 Variant 1",
      theme: ThemeData(
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage()
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
              leading: IconButton(
                      icon:Icon(Icons.chevron_left, color: Colors.white,),
                      onPressed: () {

                      },
              ),
              actions: [IconButton(
                        icon: Icon(Icons.add, color: Colors.white,),
                        onPressed: (){},
                        ),
                        IconButton(
                          icon: Icon(Icons.settings, color: Colors.white,),
                          onPressed: (){},
                        ),
              ],
              title: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("Картинки и меню",
                      style: TextStyle(fontSize: 24, color: Colors.white),
                      textAlign: TextAlign.center),
                  Padding(padding:  const EdgeInsets.only(left: 5.0),
                    child: Icon(Icons.image, color: Colors.white) ,
                  )
                ],
              ),
              centerTitle: true,
              backgroundColor: Colors.blueGrey,
    ),
      body: Stack(
          alignment: Alignment.center,
          children: [
            Align(alignment: Alignment.topLeft,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child:IconButton(
                    icon: Icon(Icons.add, color: Colors.blueGrey
                      , size: 50),
                    onPressed: (){},
              )
              ),
            ),
            Container(
              child: Image.network(
                'https://i.pinimg.com/originals/0e/9a/c9/0e9ac968707b564f4614fae264ab645c.jpg',
              ),
            ),
            ElevatedButton(
              onPressed: () {},
              child: Text('Поверх'),
              style:ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blueGrey),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                minimumSize: MaterialStateProperty.all<Size>(Size(100, 100)),
                shape: MaterialStateProperty.all<OutlinedBorder>(
                                RoundedRectangleBorder(borderRadius:
                                BorderRadius.all(Radius.circular(15.0))))
              ),

            ),
            Align(alignment: Alignment.bottomRight,
              child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child:IconButton(
                    icon: Icon(Icons.delete, color: Colors.blueGrey, size: 50),
                    onPressed: (){},
                  )
              ),
            )
          ],
      ),
    );
  }
}

class iconTitleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Icon(Icons.train,
        color: Colors.red,),
        Icon(Icons.place),
        SizedBox( width: 3),
        Text("Картики и меню",
        textAlign: TextAlign.center,)
      ],
    );
  }
}