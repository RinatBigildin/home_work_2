import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Home Work 2 Variant 3",
        theme: ThemeData(
          primarySwatch: Colors.grey,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage()
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        leading: IconButton(
        icon:Icon(Icons.list, color: Colors.white,),
        onPressed: () {
          if(scaffoldKey.currentState!.isDrawerOpen){
            scaffoldKey.currentState!.closeDrawer();
            //close drawer, if drawer is open
          }else{
            scaffoldKey.currentState!.openDrawer();
            //open drawer, if drawer is closed
          }
        },
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.add, color: Colors.white,),
            onPressed: (){},
          ),
        ],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("В контакте",
                style: TextStyle(fontSize: 24, color: Colors.white),
                textAlign: TextAlign.center),
            Padding(padding:  const EdgeInsets.only(left: 5.0),
              child: Icon(Icons.accessibility_outlined, color: Colors.white) ,
            )
          ],
        ),
        centerTitle: true,
        backgroundColor: colorButton,
      ),
      drawer: Drawer(
        child: ListView(
          children: getlistOfDraw(),
          physics: BouncingScrollPhysics(),
        ),
      ),
    );
  }
}
final scaffoldKey = GlobalKey<ScaffoldState>();

TextStyle styleOfTextTile = TextStyle(fontSize: 12);
Color colorButton = Colors.blueGrey;
double sizeIcon = 30;

List<Widget> getlistOfDraw() {
  List<Widget> list = [UserAccountsDrawerHeader(
    accountName: Text("Бигильдин Р.Э", style: TextStyle(fontSize: 16, color: Colors.white),),
    accountEmail: Text("в сети", style: TextStyle(fontSize: 12, color: Colors.white),),
    currentAccountPicture: CircleAvatar(
      backgroundImage: NetworkImage("https://cdn-icons-png.flaticon.com/512/146/146035.png"),
    ),
    decoration: BoxDecoration(
      image: DecorationImage(
        image: NetworkImage("https://look.com.ua/pic/201805/800x600/look.com.ua-280992.jpg"),
        fit: BoxFit.fill,
      ),
    ),
  )];
  list.addAll(listOfTitle);
  return list;
}

List<ListTile> listOfTitle = [
  ListTile(
    title: Text("Новости", style: styleOfTextTile),
    leading: Icon(Icons.newspaper, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
  ListTile(title: Text("Ответы", style: styleOfTextTile),
    leading: Icon(Icons.notifications, size: sizeIcon, color: colorButton),
    onTap: () { },
  ),
  ListTile(title: Text("Сообщения", style: styleOfTextTile),
    leading: Icon(Icons.mail, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
  ListTile(title: Text("Друзья", style: styleOfTextTile),
    leading: Icon(Icons.person, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
  ListTile(title: Text("Группы", style: styleOfTextTile),
    leading: Icon(Icons.people, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
  ListTile(title: Text("Фотографии", style: styleOfTextTile),
    leading: Icon(Icons.newspaper, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
  ListTile(title: Text("Видео", style: styleOfTextTile),
    leading: Icon(Icons.local_movies, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
  ListTile(title: Text("Музыка", style: styleOfTextTile),
    leading: Icon(Icons.music_note, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
  ListTile(title: Text("Документы", style: styleOfTextTile),
    leading: Icon(Icons.file_copy, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
  ListTile(title: Text("Дни рождения", style: styleOfTextTile),
    leading: Icon(Icons.cake, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
  ListTile(title: Text("Закладки", style: styleOfTextTile),
    leading: Icon(Icons.star, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
  ListTile(title: Text("Понравилось", style: styleOfTextTile),
    leading: Icon(Icons.favorite, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
  ListTile(title: Text("Выход", style: styleOfTextTile),
    leading: Icon(Icons.exit_to_app, size: sizeIcon, color: colorButton),
    onTap: () {},
  ),
];

