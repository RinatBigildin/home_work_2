import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Home Work 2 Variant 2",
        theme: ThemeData(
          primarySwatch: Colors.grey,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage()
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
          icon: Icon(Icons.add, color: Colors.white,),
          onPressed: (){},
        ),
        ],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Лист",
                style: TextStyle(fontSize: 24, color: Colors.white),
                textAlign: TextAlign.center),
            Padding(padding:  const EdgeInsets.only(left: 5.0),
              child: Icon(Icons.list, color: Colors.white) ,
            )
          ],
        ),
        centerTitle: true,
        backgroundColor: colorButton,
      ),
      body:
      ListView(
        padding: const EdgeInsets.all(4),
        children:listOfTitle,
      ),
      floatingActionButton:
        FloatingActionButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50))),
          onPressed: () {},
          child: Icon(Icons.add, color: Colors.white, size: 40,),
          backgroundColor: Colors.blueGrey,
      ),
    );
  }
}
TextStyle styleOfTextTile = TextStyle(fontSize: 24);
Color colorButton = Colors.blueGrey;


List<ListTile> listOfTitle = [
  ListTile(title: Text("Природа", style: styleOfTextTile),
            leading: Image.network("https://icons.iconarchive.com/icons/emoopo/darktheme-folder/96/Folder-Nature-Insect-icon.png"),
            trailing: Icon(Icons.image, size: 30, color: colorButton),
            onTap: () {},
  ),
  ListTile(title: Text("Камера", style: styleOfTextTile),
    leading: Image.network("https://icons.iconarchive.com/icons/nishad2m8/hologram-dock/96/Camera-icon.png"),
    trailing: Icon(Icons.camera, size: 30, color: colorButton),
    onTap: () { },
  ),
  ListTile(title: Text("Портрет", style: styleOfTextTile),
    leading: Image.network("https://icons.iconarchive.com/icons/babasse/old-school/96/fichiers-gif-icon.png"),
    trailing: Icon(Icons.portrait, size: 30, color: colorButton),
    onTap: () { },
  ),
  ListTile(title: Text("Папка", style: styleOfTextTile),
    leading: Image.network("https://icons.iconarchive.com/icons/hopstarter/soft-scraps/96/Folder-Generic-Green-icon.png"),
    trailing: Icon(Icons.folder, size: 30, color: colorButton),
    onTap: () { },
  ),
];

